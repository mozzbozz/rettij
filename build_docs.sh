#!/usr/bin/env bash

# Building the docs
if [[ -z $CI_COMMIT_TAG ]]; then CI_COMMIT_TAG=0.0.0a; fi
echo "Enter the Name of the Version you want to build (e. g. 'local' or '1.0.0')"
read myversion
echo $myversion > ./VERSION
SPHINX_APIDOC_OPTIONS="members,undoc-members,show-inheritance" sphinx-apidoc -f -o docs/source/code rettij/
sphinx-build -E -a -b html docs/source public/
