# Contributing

The following is a set of guidelines for contributing to the rettij network simulator project.

## Contributing code

### Versioning
Both the component images and the topology file format are versioned.
When changing the structure or compatibility of either one, the version must be raised.

#### Topology
The minor and major version of the supported topology file format are set in the attributes `MINOR_VERSION` and `MAJOR_VERSION` of `rettij.topology.topology_reader.TopologyReader`.
The topology versioning uses `x.x` as version numbers, meaning `major.minor`.

Added keys and possible value types will increase the minor version.
Changed and removed keys and possible values will increase the major version, as old topologies cannot be parsed anymore.

**Make sure to also update all example and testing topologies, if they are affected by any changes!**

### Issues
#### Basic notes
* When selecting a new Issue to work on, prioritize those that have the label `Bugfix` and/or are associated with the current milestone.
* You should only have one Issue assigned to yourself and labeled as `Doing` at any time. Exceptions apply.


#### Issue lifecycle
##### Creating an Issue
* Create the Issue from the `Standard` template.
* Add yourself or your supervisor as `supervisor`.
* Fill out necessary information and task list.
* Add the appropriate labels from the existing selection (do not add new labels!).

**REMEMBER**: The description should be clear and informational enough for somebody else to work on the Issue without having to consult you first!


##### Starting work an Issue
* Assign yourself to the Issue:
	```
	/assign @<username>
	```
* Label the Issue as `Doing`:
	```
	/label ~Doing
	```

##### Tracking your progress
* Whenever you encouter important information regarding your tasks, add a comment to the Issue presenting that information.
* Whenever you encounter a problem that you cannot solve right away, add a comment to the Issue describing the problem.
* At the end of the work day, add your time spent on the issue:
	```
	# Replace 1h30m with the actual time spent today.
	/spend 1h30m
	```

##### Merging your changes
* Once your work on the Issue is done, create a [Merge Request](#merge-requests).
* Remove the `Doing` label from the Issue and label it as `In Review`:
	```
	/unlabel ~Doing
	/label ~"In Review"
	```
* Once the Merge Request is accepted, the Issue will be closed automatically.

### Branches
Every new feature, bugfix or refactoring should be created in its own branch, as the default branch is protected.

In almost any case, a branch is created in order to work on an Issue.
These branches have the following naming convention that is also used by GitLab when automatically generating branches for Issues:
```
<Issue #>-<Issue title>

Example:
"75-detailed-exceptions-for-topology-validation"
```

If you just need to change a minor aspect, for example a typo or a faulty parameter, use a description of the branches purpose as the name:
```
Examples:
"bugfix-remove-faulty-value"
"fix-typo-in-readme"
```

A branch meant for resolving an issue can be created directly from the issue itself.

Branches will be deleted after they are merged into the main `develop` branch.

### Commit messages
Please write meaningful commit messages. Commits will be looked at during code review.

Follow the guideline listed below:

* Separate subject from body with a blank line
* Limit the subject line to 50 characters
* Capitalize the subject line
* Do not end the subject line with a period
* Use the imperative mood in the subject line


* Wrap the body at 72 characters
* Use the body to explain what was changed and why
* Refer to the Issue number in the body

A commit message may look like this:
```
Make SimulationManager thread safe

Adjusted all externally used methods in the SimulationManager to be
thread safe via the usage of locks. This was necessary since
finalization, which cleans up the simulation environment, can be
started while other operations are still running. This lead to various
errors as API objects expected to be present by some methods were
removed by the finalization during the methods runtime.

See #235
```

If you only made a small change that does not require further explanation, you only need to include the title and the Issue reference (if applicable).

### Merge requests
Changes are added to them main `develop` branch through merge requests.

#### Scope
As we do a code review on every merge request, please keep them as short and concise as possible.
This ensures that MRs can be reviewed and approved quickly.

If you are working on a larger feature, consider creating sub-branches for this feature and posting merge-requests to the main feature branch in smaller chunks.

#### Title
Merge requests should be titled like this:

```
[Resolve <Issue #>:] <Issue title | Description of purpose>

Example: "Resolve #153: Add execution date to log file names"
```

The `resolve` keyword tells GitLab to automatically close the associated issue.

#### Squash commits
In order to keep the commit history of the `develop` branch manageable, please activate the `Squash commits when merge request is accepted.` option and supply a short but complete description of the changes made as a new thread.

The description should always have a descriptive title.
The list of changes is best supplied as a single sentence bullet point list, but can also be in text form if that is more appropriate.
You may use the information from the associated Issue(s) for this, especially the title (as long as it is still correct).

## Style guides and conventions
To ensure consistent coding styles and high code quality, we use various static code analysis tools which are also integrated into our GitLab CI/CD pipeline.

*Make sure* to also read the [pre-commit section](#git-pre-commit-hooks) or the pipeline will most likely fail!

### Code formatting
The following code formatting conventions are set for this project:
* Line length of 120
* PEP8 conformity

You can use you can use [black](https://github.com/psf/black) to automatically reformat the project:
```bash
black -t py37 -l 120 . # auto-format
black -t py37 -l 120 --check . # test only
```

Code formatting is validated in the CI/CD pipeline using black, so please ensure that your committed code follows these guidelines. You may register black as a pre-commit hook in order to do so.

### Type hinting
We use, and will soon enforce, type hints everywhere. This reduces the likeliness of bugs and improves code readability.

Python type hints look like this:
```python
var: Type = value

size: int = 3
size_list: List[int] = [3,4,5]
```

For more information please refer to Python [typing docs](https://docs.python.org/3/library/typing.html) (even more details in [PEP 484](https://www.python.org/dev/peps/pep-0484/)).

### String interpolation
Strings and outputs containing variables should be interpolated using [f-Strings](https://www.python.org/dev/peps/pep-0498/) (e.g. `f"My value is {my_var}!"`). Refer to this [blog post](https://realpython.com/python-f-strings/#f-strings-a-new-and-improved-way-to-format-strings-in-python) for an introductory explanation of f-strings.

If variable names are too long, or operations have to be performed before creating the string, the `str.format()` method may be used (e.g. `"My value is {}".format(my_var)`).

### Docstring
Every class and method should have docstring describing its purpose as well as its parameters, return values and known possible exceptions. We currently use [Sphinx-compatible reStructuredText formatting](https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html), which is also the default setting in PyCharm.

Example:
```python
def load_json_schema(cls, schema_file_path: ValidatedFilePath) -> Dict:
	"""
	Method to load a json schema from a given filepath
	:param schema_file_path: The file path of the schema file to validate against.
	:return: The json schema as a dictionary.
	:raises: FileNotFoundError, json.decoder.JSONDecodeError
	"""
```

Additionally, any code that is not self-explanatory (multi-line statements, complex list comprehension etc.) should also be commented with a textual explanation of its purpose and structure.

### Git Pre-Commit Hooks
Enable the pre-commit hook to ensure coding standards. Otherwise, the CI pipeline will likely fail and your code will not be accepted.
This will ensure that your changes will be accepted by the pipeline.

#### Setup

This project uses the Python `pre-commit` package to simplify the setup.
Just run the following commands from a **venv-activated** shell in the `rettij` root directory to install the pre-commit hooks:
```bash
# Install pre-commit Hooks
(venv) $ pre-commit install --install-hooks --overwrite
```

You can verify the installation using the following command:
```bash
(venv) $ pre-commit run --all-files
```

To remove the pre-commit hook, run:
```bash
(venv) $ pre-commit uninstall
```

#### Checks
We are employing the following checks and formatters:
* check-ast: Check whether files parse as valid python.
* check-docstring-first: Check for a common error of placing code before the docstring.
* trailing-whitespace: Trim trailing whitespaces.
* black: Python source code auto-formatting.
* flake8: Python source code linting.
* mypy: Static type checking for Python source code.

The full configuration is stored in the [.pre-commit-config.yaml](https://gitlab.com/frihsb/rettij/-/blob/master/.pre-commit-config.yaml) file.

## Set up a development environment for rettij

Please make sure that you worked through the [Getting started tutorial](docs/getting-started.md) before.

### 1. Install git:
```bash
sudo apt install git
```

### 2. Change to the directory in which you want to install rettij, e.g.:
```bash
cd ~/git/
```

### 3. Clone the project:
```bash
# Clone the project using ssh
git clone git@gitlab.com:frihsb/rettij.git
# Go into the rettij root directory
cd rettij
```

### 4. Add the absolute path to rettij to your PYTHONPATH:
```bash
# Temporary (lasts only for the duration of the session):
export PYTHONPATH="${PYTHONPATH}:~/git/rettij/"
```
```bash
# Permanent: Open the file ~/.profile:
nano ~/.profile
# Add the following line to the end:
export PYTHONPATH="${PYTHONPATH}:~/git/rettij"
# Save and exit
```
```bash
# Windows CMD / Powershell
set PYTHONPATH=%PYTHONPATH%;%userprofile%\git\rettij
```

### 5. Create a virtual environment (venv) in your rettij repository and activate it:
```bash
# Create
python3.8 -m venv ./venv
# Activate
source venv/bin/activate
```

### 6. Install the packages required to develop rettij:
Please make sure that you are inside the venv - you will see a (venv) prefixed to your prompt in the terminal:
```bash
(venv) ~/git/rettij$
```
Install dependencies via pip3:
```bash
pip3 install -r requirements.txt
```

### 7. Test your environment with the main.py
```bash
python3 main.py <parameters>
```
