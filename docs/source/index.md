```{include} ./README.md
```

```{toctree}
---
maxdepth: 3
caption: Documentation
---
README
docs/getting-started
docs/working-with-rettij
docs/troubleshooting
docs/example
CONTRIBUTING
```

```{toctree}
---
maxdepth: 3
caption: API Docs
---
code/modules
```

