# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
import time

sys.path.insert(0, os.path.abspath("../../"))
print(sys.path)

# -- Project information -----------------------------------------------------

project = "rettij"
# "copyright" is a built in variable, but is also used by Sphinx.
# noinspection PyShadowingBuiltins
copyright = f"{time.strftime('%Y')}, FRI"  # noqa: A001
author = "FRI"

# The full version, including alpha/beta/rc tags
with open("../../VERSION", "r") as fh:
    release = fh.read().strip("\n")
    version = release

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.

extensions = ["sphinx.ext.autodoc",
              "myst_parser",
              ]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# The file extensions of source files. Sphinx considers the files with this suffix as sources.
# The value can be a dictionary mapping file extensions to file types. For example:
source_suffix = {
    '.rst': 'restructuredtext',
    '.md': 'markdown',
}

# A boolean that decides whether module names are prepended to all object names (for object types where a “module”
# of some kind is defined), e.g. for py:function directives. Default is True.
add_module_names = False

# The default options for autodoc directives. They are applied to all autodoc directives automatically.
# It must be a dictionary which maps option names to the values.
# Setting None or True to the value is equivalent to giving only the option name to the directives.
# The supported options are 'members', 'member-order', 'undoc-members', 'private-members',
# 'special-members', 'inherited-members', 'show-inheritance', 'ignore-module-all', 'imported-members'
# and 'exclude-members'.
autodoc_default_options = {
    'members': None,
    'undoc-members': None,
    'special-members': "__init__",
    'show-inheritance': True,
    'member-order': 'bysource',
    'exclude-members': '__dict__,__weakref__,__module__',
}

# This value contains a list of modules to be mocked up. This is useful when some external dependencies
# are not met at build time and break the building process. You may only specify the root package of the
# dependencies themselves and omit the sub-modules.
autodoc_mock_imports = ["readline"]

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
html_theme = "furo"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]

# Set favicon
html_favicon = 'favicon.png'

# Set top left rettij logo
html_logo = '../logos/rettij-large.png'

def setup(app):
    app.add_css_file('custom_theme.css')


# -- Options for MYST parser -------------------------------------------------
myst_heading_anchors = 5
