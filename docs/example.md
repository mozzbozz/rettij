Hands-on examples
==============

This section should serve as a reference and provide an example for how to create a custom component for rettij as well as how to write a custom sequence for said component.

## Preface
To read about what a custom component is, please consult the [Custom components for rettij section](working-with-rettij.md#custom-components-for-rettij) of our ["Working with rettij" documentation](working-with-rettij.md)  

This example shows the following scenario: You want to integrate a webserver as a Node into your rettij topology and
 be able to ping your webserver and make requests via cURL.

### Requirements
* Docker installed on your machine

### Setting up your environment

1. Create a new folder for your project.
1. Install rettij according the ["Getting Started" instructions](getting-started.md).

## Scenario 1: Standalone simulation
This hands-on example guides you through the process of creating a standalone simulation scenario including a custom component and sequence.

### Create a topology with a custom component

#### Create the custom component

##### 1. Create the folder structure
Create the appropriate folder structure in your project directory:
```
<my_project>/
    custom-components/
        apache-server/
            image/
                www/
```

##### 2. Create the image for your component
The following parts are needed to create the image:
* A webserver image -> httpd (apache server)
* Dockerfile
* Content for the webserver

Follow these steps to create your image:
1. Place your webserver content inside `image/www/`. You need at least an `index.html` file. Example:
   ```html
   <html><body><h1>My Apache Server!</h1></body></html>
   ```
1. Create a file called `Dockerfile` inside the `image` directory.
1. Put the image build steps in the `Dockerfile`:
   ```dockerfile
   # Get the latest httpd (apache) image from Docker Hub
   FROM httpd:latest

   # Copy the web contents into the image
   COPY www/* /usr/local/apache2/htdocs/

   # Install the required packages
   # iproute2 is required for rettij networking
   # iputils-ping and curl are used for this example
   RUN apt-get update && \
       apt-get install -yq iproute2 && \
       apt-get install -yq iputils-ping curl && \
       apt-get clean -yq && \
       rm -rf /var/lib/apt/lists/*
   ```
1. Build the image:
   ```bash
   docker build -t apache-server .
   ```
1. (Optional) Test the image locally
   1. Start the container using `docker run -it --rm -p 8080:80 apache-server`.
   1. Try to access the webserver from another terminal using `curl http://localhost:8080`.
1. Tag and upload the image to [Docker Hub](https://hub.docker.com) (you need an account there):
   ```bash
   # Log in to Docker Hub
   docker login

   # Tag the image with your Docker Hub username as prefix
   docker tag apache-server:latest <your-dockerhub-username>/apache-server:latest

   # Upload the image to Docker Hub
   docker push <your-dockerhub-username>/apache-server:latest
   ```
   Note: After this is done, look at: [https://hub.docker.com/repositories](https://hub.docker.com/repositories) if the upload was successful.

##### 3. Create the component template
Create the component template `apache-server/apache-server.yaml`:
```yaml
containers:
  - image: <my-dockerhub-username>/apache-server:latest
    name: apache-server
```


#### Create a topology with your custom component
In order to use the custom component with rettij, you need to include it in your topology.
Create a file `topology.yml` inside your project folder and add the contents below.

<details>

<summary>Click here to show the topology</summary>

```yaml
version: '1.0'

nodes:
  - id: apache  # Node using our custom component
    device: container
    component: apache-server
    interfaces:
      - id: i0
        channel: c0
        ip: 10.0.0.1/24
  - id: switch  # Node using a pre-defined component (switch)
    device: switch
    component: simple-switch
    interfaces:
      - id: i0
        channel: c0
      - id: i1
        channel: c1
  - id: client  # Node using a pre-defined component (client)
    device: container
    component: simple-runner
    interfaces:
      - id: i0
        channel: c1
        ip: 10.0.0.2/24
```

</details>

### Create a new simulation sequence
Scenario: The client Node wants to access the apache Node's webserver via cURL.
Create a file `sequence.py` inside your project folder and add the contents below.

<details>
<summary>Click here to show the sequence</summary>

```python
from typing import Dict

from rettij.abstract_script_sequence import AbstractScriptSequence
from rettij.simulation_manager import SimulationManager
from rettij.topology.network_components.channel import Channel
from rettij.topology.node_container import NodeContainer


class ScriptSequence(AbstractScriptSequence):
    """
    This sequence defines a ping and webserver request between two nodes.
    """

    def define(
            self,
            sm: SimulationManager,
            node: NodeContainer,
            nodes: NodeContainer,
            channel: Dict[str, Channel],
            channels: Dict[str, Channel],
    ) -> None:
        """
        Define the sequential / script simulation sequence to be run.

        Add commands like you would in the rettij CLI:

        .. code-block:: python

            nodes.n1.ping(nodes.n2.ifaces.i0.ip)

        All parameters are automatically supplied upon execution by rettij.

        :param sm: SimulationManager controlling the simulation.
        :param node: NodeContainer object with all simulation Nodes (alternative to 'nodes', same object referenced).
        :param nodes: NodeContainer object with all simulation Nodes (alternative to 'node', same object referenced).
        :param channel: Map of all simulation Channels (alternative to 'channels', same object referenced).
        :param channels: Map of all simulation Channels (alternative to 'channel', same object referenced).
        """
        # Ping 'apache' Node from 'client' Node
        print(nodes.client.ping(nodes.apache.ifaces.i0.ip, c=3))

        # Access webserver on 'apache' Node from 'client' Node
        # Run cURL in silent mode to avoid outputting stats to STDERR
        print(nodes.client.run("curl -s -S http://10.0.0.1"))
```

</details>

### Use the topology and sequence with rettij
Run the following command to use the topology and sequence with rettij.
Make sure that the Virtual Environment is active!

```bash
rettij -t $PWD/topology.yml -s $PWD/sequence.py --components $PWD/custom-components/
```


## Scenario 2: Co-Simulation
This hands-on example guides you through the process of creating a simple co-simulation scenario.

Coming Soon <sup>TM</sup>

<!-- TODO: Add co-simulation scenario -->  
