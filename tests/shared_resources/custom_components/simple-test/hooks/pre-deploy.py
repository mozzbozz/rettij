from typing import Dict

from rettij.topology.hooks.abstract_pre_deploy_hook import AbstractPreDeployHook
from rettij.topology.network_components.node import Node


class PreDeployHook(AbstractPreDeployHook):
    """
    This class defines a 'pre-connect' hook for testing purposes.
    """

    def execute(self, node: Node, nodes: Dict[str, Node]) -> None:
        """
        Execute the 'pre-deploy' hook for testing purposes.
        """
        print(f"Pre-deploy hook of node {node.name}.")
