from rettij.topology.hooks.abstract_post_deploy_hook import AbstractPostDeployHook
from rettij.topology.network_components.node import Node
from rettij.topology.node_container import NodeContainer


class PostDeployHook(AbstractPostDeployHook):
    """
    This class defines a 'post-deploy' hook for testing purposes.
    """

    def execute(self, node: Node, nodes: NodeContainer) -> None:
        """
        Execute the 'post-deploy' hook for testing purposes.
        """
        print(f"Post-deploy hook of node {node.name}.")
