from typing import Dict

from rettij.topology.hooks.abstract_pre_teardown_hook import AbstractPreTeardownHook
from rettij.topology.network_components.node import Node


class PreTeardownHook(AbstractPreTeardownHook):
    """
    This class defines a 'pre-teardown' hook for testing purposes.
    """

    def execute(self, node: Node, nodes: Dict[str, Node]) -> None:
        """
        Execute the 'pre-teardown' hook for testing purposes.
        """
        print(f"Pre-teardown hook of node {node.name}.")
