from ipaddress import IPv4Address
from typing import Dict

from rettij.abstract_script_sequence import AbstractScriptSequence
from rettij.simulation_manager import SimulationManager
from rettij.topology.network_components.channel import Channel
from rettij.topology.node_container import NodeContainer


class ScriptSequence(AbstractScriptSequence):
    """
    This class defines a simple scripted ping sequence to go along with 'tests/src/integration_tests/test_rettij/resources/topology.yml'.
    """

    def define(
        self,
        sm: SimulationManager,
        node: NodeContainer,
        nodes: NodeContainer,
        channel: Dict[str, Channel],
        channels: Dict[str, Channel],
    ) -> None:
        """
        Define the sequential / script simulation sequence to be run.

        Add commands like you would in the rettij CLI:

        .. code-block:: python

            nodes.n1.ping(nodes.n2.ifaces.i0.ip)

        All parameters are automatically supplied upon execution by rettij.
        :param sm: SimulationManager controlling the simulation.
        :param node: NodeContainer object with all simulation Nodes (alternative to 'nodes', same object referenced).
        :param nodes: NodeContainer object with all simulation Nodes (alternative to 'node', same object referenced).
        :param channel: Map of all simulation Channels (alternative to 'channels', same object referenced).
        :param channels: Map of all simulation Channels (alternative to 'channel', same object referenced).
        """
        client01_ip: IPv4Address = nodes["client01"].ifaces["i0"].ip
        client03_ip: IPv4Address = nodes["client03"].ifaces["i0"].ip

        nodes.client01.ping(target=client01_ip.compressed, c=1)
        nodes.client01.ping(target=client03_ip.compressed, c=1)
