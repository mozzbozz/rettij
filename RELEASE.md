# Release a new rettij Version

Releases are automatically handled by the gitlab CI. A new release is triggered when a new tag is created. Only maintainers can create new releases.

1. Go to https://gitlab.com/frihsb/rettij/-/tags/new
1. Fill in the tag information
   1. `Tag name`: The rettij version number. rettij uses _semantic versioning_ (https://semver.org/).
   1. `Create from`: `master`
   1. `Message`: Depending on major, minor or bugfix version upgrade.
      1. Major: `Breaking changes, new features and bugfixes`
      1. Minor: `New features and bugfixes`
      1. Bugfix: `Bugfixes`
   1. `Release notes`: A short and concise overview of new or changed features and documentations that are relevant to the user.
1. Create the tag. Once the associated pipeline has finished successfully, the release package will be available at https://gitlab.com/frihsb/rettij/-/packages and https://pypi.org/project/rettij/.

## Release Note Template

```markdown
# Release notes
## Features

- new_or_changed_feature_1
- new_or_changed_feature_2

## Docs

- new_or_changed_documentation_1
- new_or_changed_documentation_2
```
