"""
This script is only meant for local development and is not included in the built package.

To run rettij once it is installed via pip, simply use `python -m rettij`.
"""
import os
import sys

from rettij import standalone

project_root: str = os.path.dirname(os.path.realpath(__file__))
rettij_dir: str = os.path.join(project_root, "rettij")
sys.path.insert(0, rettij_dir)

standalone()
