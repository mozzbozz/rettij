#!/bin/bash

# Purpose: Build bridge image and push it to a registry
# Usage: build_simpleswitch.sh <repository>

# "frihsb/rettij"

function build {
  repository=$1

  if [ -f "Dockerfile" ]
  then
    export DOCKER_BUILDKIT=1
    version=$(cat VERSION)
    docker build -t "$repository"_simple-switch:latest .
    docker build -t "$repository"_simple-switch:"$version" .
    docker push "$repository"_simple-switch:latest
    docker push "$repository"_simple-switch:"$version"

    docker image prune -f

  fi
}

cd "$(dirname "$0")" || exit

if [ "${#}" -eq 1 ]; then
  build "$1"
elif [ "${#}" -eq 0 ]; then
  echo "Pushing to frihsb/rettij"
  build "frihsb/rettij"
elif [ "${#}" -gt 1 ]; then
  echo "Error: Too many arguments!"
  echo "Usage: build_simpleswitch.sh <repository>"
fi
